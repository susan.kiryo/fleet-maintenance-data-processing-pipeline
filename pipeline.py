import json
import pandas as pd
from logging_config import setup_logging
from quality_checks import count_duplicates, missing_data, check_truck_ids
from processing import merge_dataframes, standardize_service_type, one_hot_encode, validate_emails, format_dates, calculate_maintenance_cost_per_km

def main():
    # Setup logging for different types of logs
    app_logger, dq_logger, info_logger = setup_logging()

    # Load configuration from JSON file
    try:
        with open('data_sources.json', 'r') as file:
            config = json.load(file)
        info_logger.info("Loaded configuration successfully.")
    except FileNotFoundError as e:
        app_logger.error(f"Failed to load configuration file: {e}")
        return # Stop execution if config not found

    # Load data from CSV files specified in the configuration
    try:
        datasets = {}
        for dataset in config['datasets']:
            path = dataset['path']
            dtype = dataset['type']
            datasets[dtype] = pd.read_csv(path)
            info_logger.info(f"Loaded {dtype} data from {path}")
    except Exception as e:
        app_logger.error(f"Error loading data files: {e}")
        return # Stop execution if data cannot be loaded

    # Perform quality checks and data processing
    try:
        # Check and log duplicate records
        for dtype, df in datasets.items():
            duplicates = count_duplicates(df)
            if duplicates > 0:
                dq_logger.info(f"Duplicate records found for {dtype} data: {duplicates}\n")
                # Remove duplicates
                df.drop_duplicates(inplace=True) # Remove duplicates

        # Check and log missing data
        for dtype, df in datasets.items():
            missing_counts = missing_data(df)
            if not missing_counts.empty:
                formatted_missing = missing_counts.to_string(index=True, header=False)
                dq_logger.info(f"Missing Data in '{dtype}':\n{formatted_missing}\n")

        # Process data further if both datasets are available
        if 'fleet' in datasets and 'maintenance' in datasets:
            merged_df = merge_dataframes(datasets['fleet'], datasets['maintenance'])
            standardize_service_type(merged_df)
            merged_df = one_hot_encode(merged_df)
            validate_emails(merged_df)
            format_dates(merged_df, ['purchase_date', 'maintenance_date'])
            calculate_maintenance_cost_per_km(merged_df)

            # # Log IDs missing from datasets
            missing_in_fleet, missing_in_maintenance = check_truck_ids(datasets['maintenance'], datasets['fleet'])
            # Log missing truck IDs in fleet
            if missing_in_fleet:
                dq_logger.info(f"Truck IDs missing from the fleet dataset: {', '.join(map(str, missing_in_fleet))}\n")
                # Log missing truck IDs in maintenanc
                if missing_in_maintenance:
                    dq_logger.info(f"Truck IDs missing from the maintenance dataset: {', '.join(map(str, missing_in_maintenance))}\n")

        # Save the final merged and processed DataFrame
        merged_df.to_csv('fleet_maintenance_MERGED.csv', index=False)
        info_logger.info("Merged and processed data saved to 'fleet_maintenance_MERGED.csv'\n")

    except Exception as e:
        app_logger.error(f"Error during data processing: {e}\n")

if __name__ == "__main__":
    main()
