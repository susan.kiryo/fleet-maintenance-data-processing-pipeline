# Fleet Maintenance Data Processing Pipeline

## Project Overview
This project implements a data processing pipeline for fleet and maintenance data. It merges, cleans, and enhances datasets to prepare them for analysis, focusing on calculating maintenance costs and ensuring data quality.

## Setup
### Requirements
- Python 3.x
- Install all necessary Python packages using:
  pip install -r XXX

### Configuration
- Ensure that you have your data and scripts configured with the correct paths.

your_project/
│
├── src/
│   ├── __init__.py       # Ensures Python treats the src directory as a package
│   ├── logging_config.py
│   ├── pipeline.py
│   ├── processing.py
│   ├── quality_checks.py
│
├── data/
│   ├── fleet_information.csv
│   ├── maintenance_records.csv
│
├── logs/
│   ├── application_errors.log
│   ├── data_quality.log
│   ├── info.log
│
├── data_sources.json
├── fleet_maintenance_MERGED.csv
├── README.md


## Running the Script
To run the script, use the following command from the project root directory:

python src/pipeline.py


## Authors

[Susan Kiryo](gitlab.com/susan.kiryo)



## Acknowledgments

Thanks to the Nick for the great lecturers!
