import logging

def setup_logging():
    # Application errors logger
    app_logger = logging.getLogger('application_errors') # Creating a logger
    app_handler = logging.FileHandler('logs/application_errors.log') # Creating a file handler
    app_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s') # Defining log message format
    app_handler.setFormatter(app_formatter) # Set the formatter for this handler
    app_logger.addHandler(app_handler) # Add the specified handler to this logger
    app_logger.setLevel(logging.ERROR) # Set the logging level of this logger

    # Data quality logger
    dq_logger = logging.getLogger('data_quality') # Creating a logger
    dq_handler = logging.FileHandler('logs/data_quality.log') # Creating a file handler
    dq_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s') # Defining log message format
    dq_handler.setFormatter(dq_formatter) # Set the formatter for this handler
    dq_logger.addHandler(dq_handler) # Add the specified handler to this logger
    dq_logger.setLevel(logging.INFO) # Set the logging level of this logger
    
    # Info logger
    info_logger = logging.getLogger('info') # Creating a logger
    info_handler = logging.FileHandler('logs/info.log') # Creating a file handler
    info_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s') # Defining log message format
    info_handler.setFormatter(info_formatter) # Set the formatter for this handler
    info_logger.addHandler(info_handler) # Add the specified handler to this logger
    info_logger.setLevel(logging.INFO) # Set the logging level of this logger

    return app_logger, dq_logger, info_logger