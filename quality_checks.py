import pandas as pd
import re

# Data Quality Checks
def count_duplicates(df):
    """Return the number of duplicate rows in the DataFrame."""
    return df.duplicated().sum()

def missing_data(df):
    """Return a Series with counts of missing values for columns with any missing data."""
    return df.isnull().sum()[df.isnull().sum() > 0]

def check_truck_ids(fleet_df, maintenance_df):
    """
    Returns two lists:
    1. Truck IDs present in maintenance data but missing from fleet data.
    2. Truck IDs present in fleet data but missing from maintenance data.

    Parameters:
    - fleet_df (pandas.DataFrame): DataFrame containing fleet data.
    - maintenance_df (pandas.DataFrame): DataFrame containing maintenance data.

    Returns:
    - tuple of two lists: (missing_in_fleet, missing_in_maintenance)
    """
    fleet_ids = set(fleet_df['truck_id'])
    maintenance_ids = set(maintenance_df['truck_id'])

    missing_in_fleet = list(maintenance_ids - fleet_ids)
    missing_in_maintenance = list(fleet_ids - maintenance_ids)

    return missing_in_fleet, missing_in_maintenance

def is_valid_email(email):
    """Check if the email address is in a valid format, and Detect missing values as NaN."""
    if pd.isna(email):
        return False
    return bool(re.match(r"[^@]+@[^@]+\.[^@]+", email))