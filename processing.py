import pandas as pd
from quality_checks import is_valid_email

# Data Processing Functions
def merge_dataframes(fleet_df, maintenance_df):
    """Merge fleet and maintenance data on the 'truck_id' column."""
    return pd.merge(fleet_df, maintenance_df, on='truck_id')

def standardize_service_type(df):
    """Standardize the 'service_type' column to lower case and strip whitespaces, Check and convert non-string inputs to strings."""
    df['service_type'] = df['service_type'].astype(str).str.lower().str.strip()

def one_hot_encode(df):
    """Apply one-hot encoding to the 'service_type' column."""
    return pd.get_dummies(df, columns=['service_type'])

def validate_emails(df):
    """Add a 'valid_email' column indicating if 'technician_email' is valid."""
    df['valid_email'] = df['technician_email'].apply(is_valid_email)

def format_dates(df, date_columns):
    """Convert date columns to a specified format, setting any unconvertible values to NaT."""
    for column in date_columns:
        df[column] = pd.to_datetime(df[column], format='%d-%m-%Y', errors='coerce')

def calculate_maintenance_cost_per_km(df, cost_col='maintenance_cost', current_milage_col='milage', initial_milage_col='initial_milage'):
    """Calculate maintenance cost per kilometer based on the formula given."""
    # Replace NaN in current_mileage_col with initial_mileage_col
    df[current_milage_col] = df[current_milage_col].fillna(df[initial_milage_col])

    # Calculate cost per km ensuring no division by zero
    """ 'if(row...' Checks if the mileage difference is greater than zero to avoid division by zero. If it is True, the cost per km will be set to 0."""
    df['maintenance_cost_per_km'] = df.apply(
        lambda row: round(row[cost_col] / (row[current_milage_col] - row[initial_milage_col]), 3) if (row[current_milage_col] - row[initial_milage_col]) > 0 else 0,
        axis=1
    )